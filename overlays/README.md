# Overlays

| Package Name | Used (if not, why)     | Description                                      |
|--------------|------------------------|--------------------------------------------------|
| `eww-git`    | No, flake used instead | `ralismark/tray-3` repo, adding systray support. |
